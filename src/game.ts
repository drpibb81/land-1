/////////////////////////////////////////////////////////
// demo-BuilderHUD
// (c) 2019 Carl Fravel
/////////////////////////////////////////////////////////

//import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'

//////////////////////////////////////////
// Entities

// This scene entity wraps the whole scene, so it can be moved, rotated, or sized as may be needed
const scene = new Entity()
//@CF TODO i set pos != 0,0,0 to test parent effect
const scenetransform = new Transform({ position: new Vector3(0, 0, 0), rotation: Quaternion.Euler(0, 0, 0), scale: new Vector3(1, 1, 1) })
scene.addComponent(scenetransform)
engine.addEntity(scene)

let grass = new Entity()
let grassShape = new  BoxShape()
grass.addComponent(grassShape)
let grassTexture = new Texture('materials/Tileable-Textures/grassy-512-1-0.png')
let grassMaterial = new Material()
grassMaterial.albedoTexture = grassTexture
grass.addComponent(grassMaterial)
let grassTransform = new Transform({position: new Vector3(8,0,8), scale: new Vector3(16,0.01,16)})
grass.addComponent(grassTransform)
grass.setParent(scene)

const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)



/// --- Set up a system ---

class RotatorSystem {
    // this group will contain every entity that has a Transform component
    group = engine.getComponentGroup(Transform)
  
    update(dt: number) {
      // iterate over the entities of the group
      for (let entity of this.group.entities) {
        // get the Transform component of the entity
        const transform = entity.getComponent(Transform)
  
        // mutate the rotation
        //transform.rotate(Vector3.Up(), dt * 10)
      }
    }
  }
  
  // Add a new instance of the system to the engine
  engine.addSystem(new RotatorSystem())
  
  /// --- Spawner function ---
  
  function spawnCube(x: number, y: number, z: number) {
    // create the entity
    const cube = new Entity()
  
    const myMaterial = new Material()
    myMaterial.albedoColor = Color3.Random()
    cube.addComponent(myMaterial)

    const cube10 = new Entity()
    
    const myMaterial10 = new Material()
    myMaterial10.albedoColor = Color3.Random()
    cube10.addComponent(myMaterial10)
  
    // add a transform to the entity
    cube10.addComponent(new Transform({ position: new Vector3(8, 1, 7), scale: new Vector3(1, 1, 1) }))
  
    // add a shape to the entity
    cube10.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube10)
  
    // add a transform to the entity
    cube.addComponent(new Transform({ position: new Vector3(8, 2, 8), scale: new Vector3(0.9, 0.9, 0.9) }))

    // add a shape to the entity
    cube.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube)
  
    const cube2 = new Entity()
  
    const myMaterial2 = new Material()
    myMaterial2.albedoColor = Color3.Random()
    cube2.addComponent(myMaterial2)
  
    // add a transform to the entity
    cube2.addComponent(new Transform({ position: new Vector3(8, 3, 9), scale: new Vector3(0.81, 0.81, 0.81) }))
  
    // add a shape to the entity
    cube2.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube2)
  
    const cube3 = new Entity()
  
    const myMaterial3 = new Material()
    myMaterial3.albedoColor = Color3.Random()
    cube3.addComponent(myMaterial3)
  
    // add a transform to the entity
    cube3.addComponent(new Transform({ position: new Vector3(8, 4, 10), scale: new Vector3(0.729, 0.729, 0.729) }))
  
    // add a shape to the entity
    cube3.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube3)

    const cube8 = new Entity()
  
    const myMaterial8 = new Material()
    myMaterial8.albedoColor = Color3.Random()
    cube8.addComponent(myMaterial8)
  
    // add a transform to the entity
    cube8.addComponent(new Transform({ position: new Vector3(8, 4, 11), scale: new Vector3(0.6561, 0.6561, 0.6561) }))
  
    // add a shape to the entity
    cube8.addComponent(new BoxShape())
    // add the entity to the engine
    engine.addEntity(cube8)
  
    const cube9 = new Entity()
  
    const myMaterial9 = new Material()
    myMaterial9.albedoColor = Color3.Random()
    cube9.addComponent(myMaterial9)
  
    // add a transform to the entity
    cube9.addComponent(new Transform({ position: new Vector3(10, 5, 11), scale: new Vector3(0.5314, 0.5314, 0.5314) }))
  
    // add a shape to the entity
    cube9.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube9)
  
      // add the entity to the engine
      engine.addEntity(cube9)
  
      const cube4 = new Entity()
  
      const myMaterial4 = new Material()
      myMaterial4.albedoColor = Color3.Random()
      cube4.addComponent(myMaterial4)
    
      // add a transform to the entity
      cube4.addComponent(new Transform({ position: new Vector3(9, 5, 11), scale: new Vector3(0.5904, 0.5904, 0.5904) }))
    
      // add a shape to the entity
      cube4.addComponent(new BoxShape())
    
      // add the entity to the engine
      engine.addEntity(cube4)
  
  
    const cube5 = new Entity()
  
    const myMaterial5 = new Material()
    myMaterial5.albedoColor = Color3.Random()
    cube5.addComponent(myMaterial5)
  
    // add a transform to the entity
    cube5.addComponent(new Transform({ position: new Vector3(10, 6, 12), scale: new Vector3(.4782, 0.4782, 0.4782) }))
  
    // add a shape to the entity
    cube5.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube5)
  
    const cube6 = new Entity()
  
    const myMaterial6 = new Material()
    myMaterial6.albedoColor = Color3.Random()
    cube6.addComponent(myMaterial6)
  
    // add a transform to the entity
    cube6.addComponent(new Transform({ position: new Vector3(10, 7, 13), scale: new Vector3(0.4305, 0.4305, 0.4305) }))
  
    // add a shape to the entity
    cube6.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube6)
  
    const cube7 = new Entity()
  
    const myMaterial7 = new Material()
    myMaterial7.albedoColor = Color3.Random()
    cube7.addComponent(myMaterial7)
  
    // add a transform to the entity
    cube7.addComponent(new Transform({ position: new Vector3(10, 8, 14), scale: new Vector3(0.3875, 0.3875, 0.3875) }))
  
    // add a shape to the entity
    cube7.addComponent(new BoxShape())
  
    // add the entity to the engine
    engine.addEntity(cube7)
  
     
    return cube
  
  }
  
  /// --- Spawn a cube ---
  
  const cube = spawnCube(8, 1, 8)
  
  cube.addComponent(
    new OnClick(() => {
      cube.getComponent(Transform).scale.z *= 1.1
      cube.getComponent(Transform).scale.y *= 0.9
      cube.getComponent(Transform).scale.x *= 0.9
  
    
    })
  )

  
const jetpack_01 = new Entity()
jetpack_01.setParent(scene)
const gltfShape_7 = new GLTFShape('models/Jetpack/Jetpack_01.glb')
jetpack_01.addComponentOrReplace(gltfShape_7)
const transform_10 = new Transform({
  position: new Vector3(9.6, 0, 7.16),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(.5, .5, .5)
})
jetpack_01.addComponentOrReplace(transform_10)
engine.addEntity(jetpack_01)

const Sword = new Entity()
Sword.setParent(scene)
const gltfShape_2 = new GLTFShape('models/Sword.glb')
Sword.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(10, 8.7, 14),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(.1, .1, .1)
})
Sword.addComponentOrReplace(transform_3)
engine.addEntity(Sword)

const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(jetpack_01)
hud.attachToEntity(Sword)
hud.setDefaultParent(scene)
